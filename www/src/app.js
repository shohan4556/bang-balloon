(function () {

    var game = new Phaser.Game(window.innerWidth * window.devicePixelRatio, window.innerHeight * window.devicePixelRatio, Phaser.CANVAS, 'game');
    
    
    //  Add the States your game has.
    //  You don't have to do this in the html, it could be done in your Game state too, but for simplicity I'll keep it here.
    // I think is the ok
    game.global = {
        scaleRation: 1,
        score: 00,
        lifeCount : 3,
        gamePlayMusic : null,
        VELOCITY_Y : -200,
        level_01 : true,
        level_02 : false,
    }
    // asset scale ration 
    game.global.scaleRation = window.devicePixelRatio / 3 ;
    // instansiate new phaser slider plugin
    
    //console.log(window.devicePixelRatio);

    game.state.add('Game', BasicGame.Game);
    game.state.add('Preload', BasicGame.Preload);
    game.state.add('MainScreen', BasicGame.MainScreen);
    game.state.add('LevelSelect', BasicGame.LevelSelect);
    game.state.add('Play', BasicGame.Play);
    game.state.add('Gameover', BasicGame.Gameover);
    game.state.add('Credit', BasicGame.Credit);
    game.state.add('Help', BasicGame.Help);
    game.state.add('Score', BasicGame.Score);
    
    //  Now start the Game state.
    console.log(game);
    game.state.start('Game');
    

})();