BasicGame.LevelSelect = function (game) {
    this.game = game;
    this.flag = false;
}

BasicGame.LevelSelect.prototype.preload = function () {
    console.log(this.game.state.getCurrentState());

    //this.slider = new phaseSlider(this.game); //make sure to have slider publicly available
}

BasicGame.LevelSelect.prototype.create = function () {
    //console.log(this.game.state.getCurrentState());

    this.game.stage.backgroundColor = 0xcdcdcd;

    this.blockGroup = this.game.add.group();
    var centerX = this.game.world.centerX;
    var centerY = this.game.world.centerY;

    var block1 = this.game.add.image(centerX, centerY, "level_01");
    block1.anchor.setTo(0.5);
    block1.scale.setTo(this.game.global.scaleRation);
    block1.inputEnabled = true;
    block1.events.onInputDown.add(this.level_01Tap, this);
    this.blockGroup.add(block1);

    this._width = block1.width + centerX;

    var block2 = this.game.add.image(this._width, centerY, 'level_02');
    block2.anchor.setTo(0.5);
    block2.scale.setTo(this.game.global.scaleRation);
    block2.visible = false;
    block2.inputEnabled = true;
    block2.events.onInputDown.add(this.level_02Tap, this);
    this.blockGroup.add(block2);

    this.blockGroup.setAll('alpha', 0.8);

    this.handleArrow();

    this.lockedLevel();
}

BasicGame.LevelSelect.prototype.level_01Tap = function () {
    if (this.game.global.level_01) {
        this.game.state.start('Play');
    }
}

BasicGame.LevelSelect.prototype.level_02Tap = function () {
    if (this.game.global.level_02) {
        this.game.state.start('Play');
    } else {
        console.log('level 02 locked');
    }
}

BasicGame.LevelSelect.prototype.lockedLevel = function () {
    this.locksprite = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'locked');
    this.locksprite.anchor.setTo(0.5);

    var level2 = this.blockGroup.getTop();
    level2.addChild(this.locksprite);
    this.locksprite.position.setTo(this.game.world.centeX, 100);

    // unlocked 
    if (this.game.global.level_02 == true) {
        level2.removeChild(this.locksprite);
        this.locksprite.kill();
    }
}

BasicGame.LevelSelect.prototype.handleArrow = function () {

    var arrowRight = this.game.add.responsiveImage(5 * this.game.global.scaleRation, -150, 'arrow2', null, Fabrique.PinnedPosition.bottomCenter);
    arrowRight.anchor.setTo(0.5);
    arrowRight.scale.setTo(this.game.global.scaleRation);
    arrowRight.inputEnabled = true;
    arrowRight.events.onInputDown.add(this.moveRight, this, null, arrowRight);

}


BasicGame.LevelSelect.prototype.moveRight = function (arrow) {
    console.log('right');

    this.flag = !this.flag; // switch

    var block2 = this.blockGroup.getTop();
    block2.visible = true;
    var block1 = this.blockGroup.getBottom();
    block1.visible = true;

    if (this.flag) {
        arrow.scale.x = -this.game.global.scaleRation;

        var b1_tween = this.game.add.tween(block1).to({
            x: -this._width
        }, 100, Phaser.Easing.Linear.Out, true);

        b1_tween.onComplete.add(function () {
            this.tweenToCenter(block2);
            block1.visible = false;
        }, this);

    } else {
        arrow.scale.x = this.game.global.scaleRation;

        var b2_tween = this.game.add.tween(block2).to({
            x: this._width
        }, 100, Phaser.Easing.Linear.Out, true);

        b2_tween.onComplete.add(function () {
            this.tweenToCenter(block1);
            block2.visible = false;
        }, this);
    }

}

BasicGame.LevelSelect.prototype.tweenToCenter = function (sprite) {
    var tweenCenter = this.game.add.tween(sprite);

    tweenCenter.to({
        x: this.game.world.centerX
    }, 400, Phaser.Easing.Linear.In, true);
}