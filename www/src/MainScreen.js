BasicGame.MainScreen = function (game) {
    this.game = game;
}

BasicGame.MainScreen.prototype.create = function () {
    console.log(this.state.getCurrentState());

    this.stage.backgroundColor = '#539AD2';

    // background image
    var bg = this.add.sprite(this.world.centerX, this.world.centerY, 'title-bg');
    bg.anchor.setTo(0.5);
    bg.scale.setTo(this.game.global.scaleRation);

    // clouds 
    this.clouds();

    // game title sprite
    sprite = this.add.sprite(this.world.centerX, this.world.centerY - 350 * this.game.global.scaleRation, 'game-title');
    sprite.anchor.setTo(0.5);
    sprite.scale.setTo(this.game.global.scaleRation);

    // play button
    var sprite = this.add.button(this.world.centerX, this.world.centerY + 100 * this.game.global.scaleRation, 'playButton', this.startGame, this);
    sprite.anchor.setTo(0.5);
    sprite.scale.setTo(this.game.global.scaleRation + 0.4);
    this.addTween(sprite);

    // score button
    sprite = this.add.button(this.world.centerX, this.world.centerY + 350 * this.game.global.scaleRation, 'leaderboard', this.leaderBoard, this);
    this.defaultParam(sprite, this.game.global.scaleRation);

    // help button
    sprite = this.add.button(this.world.centerX, this.world.centerY + 470 * this.game.global.scaleRation, 'help', this.gameHelp, this);
    this.defaultParam(sprite, this.game.global.scaleRation);

    // credit button
    sprite = this.add.button(this.world.centerX, this.world.centerY + 590 * this.game.global.scaleRation, 'about', this.credit, this);
    this.defaultParam(sprite, this.game.global.scaleRation);

}

BasicGame.MainScreen.prototype.clouds = function () {
    //cloud top right
    var cloud = this.add.sprite(this.world.centerX + 200 * this.game.global.scaleRation, 400 * this.game.global.scaleRation, 'cloud1');
    cloud.anchor.setTo(0.5);
    cloud.scale.setTo(this.game.global.scaleRation);

    this.addTween(cloud);

    // cloud 
    cloud = this.add.sprite(this.world.centerX - 200 * this.game.global.scaleRation, 200 * this.game.global.scaleRation, 'cloud3');
    this.defaultParam(cloud, this.game.global.scaleRation);

    this.addTween(cloud);

}

BasicGame.MainScreen.prototype.defaultParam = function (sprite, scale) {
    sprite.anchor.setTo(0.5);
    sprite.scale.setTo(scale);
}

// add some tween
BasicGame.MainScreen.prototype.addTween = function (sprite) {
    var tween = this.game.add.tween(sprite);

    sprite.angle = (2 + Math.random() * 5) * (Math.random() > 0.5 ? 1 : -1);
    var tweenPosition = this.game.add.tween(sprite);

    tweenPosition.to({
        angle: -sprite.angle
    }, 5000, Phaser.Easing.Linear.None, true, 0, -1)

    tweenPosition.yoyo(true);
}

// leaderboard callback
BasicGame.MainScreen.prototype.leaderBoard = function () {
    this.game.state.start('Score');
}

// credit 
BasicGame.MainScreen.prototype.credit = function () {
    this.game.state.start('Credit');
}

// settings
BasicGame.MainScreen.prototype.gameHelp = function () {
    this.game.state.start('Help');
}

// start game 
BasicGame.MainScreen.prototype.startGame = function () {
    this.game.state.start('LevelSelect');
    //this.game.state.start('Play');
}