 BasicGame.Credit = function (game) {
     this.game = game;
 }


 BasicGame.Credit.prototype.create = function () {
     console.log(this.game.state.getCurrentState());

     this.game.stage.backgroundColor = '#000000';

     var creditText = {
         dev: {
             name: 'Shohanur Rahaman',
             mail: 'shohan4556@gmail.com',
             git: 'shohan4556',
             blog: 'naivedev.blogspot.com'
         },
         sound: {
             name: 'Eric Matyas',
             site: 'soundimage.org'
         },
         art: {
             site1: 'opengameart.org',
             site2: 'freepik.com'
         }
     };

     var text = this.game.add.bitmapText(this.game.world.centerX, this.game.world.centerY - 200, 'font3', 'Developer : ' + creditText.dev.name + '\nMail : ' + creditText.dev.mail + '\nGithub : ' + creditText.dev.git + '\nBlog : ' + creditText.dev.blog + '\n\nSound : ' + creditText.sound.name + '\nWebsite : ' + creditText.sound.site + '\n\nArt : ' + creditText.art.site1 + '\nArt : ' + creditText.art.site2, 80);


     this.addProperty(text);
     text.align = 'center';
     text.maxWidth = this.game.world.width - 20;


     var button = this.game.add.responsiveButton(-5, -200 * this.game.global.scaleRation, 'menu', this.MainMenu, this, 0, 0, 0, 0, Fabrique.PinnedPosition.bottomCenter);
     button.anchor.setTo(0.5);
     button.scale.setTo(this.game.global.scaleRation + 0.3);
 }

 BasicGame.Credit.prototype.MainMenu = function () {
     this.game.state.start('MainScreen');
 }

 BasicGame.Credit.prototype.addProperty = function (text) {
     text.anchor.setTo(0.5);
     text.scale.setTo(this.game.global.scaleRation);
 }