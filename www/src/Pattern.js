BasicGame.Pattern = function (game) {
    // instance
    this.game = game;
}

BasicGame.Pattern.prototype.create = function () {
    this.X1 = [300, 580, 200];
    this.Y1 = [-20, 380, -150];
}

BasicGame.Pattern.prototype._Pattern1 = function (sprite) {
    
    var ax = [150,350];
    var ay = [20,380];
    
    var tween = this.game.add.tween(sprite).to({
        x: ax,
        y: ay
    },4000);
    
    tween.interpolation(Phaser.Math.bezierInterpolation);
    tween.start();
}

BasicGame.Pattern.prototype._Pattern2 = function(sprite){
    this.tween2 = this.game.add.tween(sprite).to({
        x:[500,400,300,200,100,50,100,150,250,300,350,300,250],
        y:[550,500,450,400,350,300,250,200,150,100,50,-50,-150]
    },3000);
    
    this.tween2.interpolation(Phaser.Math.bezierInterpolation);
    this.tween2.start();
}