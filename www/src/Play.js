BasicGame.Play = function (game) {
    // game widgets
    this.lifeSpriteGroup = null;
    this.game = game;
    this.SCORE = 0;
}

// main method
BasicGame.Play.prototype.create = function () {
    console.log(this.state.getCurrentState());
    this.backgroundSprite();
    this.game.physics.startSystem(Phaser.Physics.ARCADE);

    // add life
    this.lifeAdd();
    // game core here
    this.ManageBalloon();
}

BasicGame.Play.prototype.backgroundSprite = function () {
    var sprite = this.add.sprite(this.world.centerX, this.world.centerY, 'level_01_bg');
    sprite.anchor.setTo(0.5);
    sprite.scale.setTo(this.game.global.scaleRation);
}

// instantiate and call Balloon.js 
BasicGame.Play.prototype.ManageBalloon = function () {
    
    this.Balloons = new BasicGame.Ballons(this.game);
    this.Balloons.create(this.lifeSpriteGroup);
}

BasicGame.Play.prototype.update = function () {
    this.Balloons.update(); // calls ballons update
}

// lifesprite added
BasicGame.Play.prototype.lifeAdd = function () {
    this.lifeSpriteGroup = this.add.group();

    // life 1
    var sprite = this.add.responsiveImage(-65 * this.game.global.scaleRation, 60 * this.game.global.scaleRation, 'life', null, Fabrique.PinnedPosition.topRight);
    this.defaultParam(sprite, this.game.global.scaleRation);
    this.lifeSpriteGroup.add(sprite);

    // life 2
    var sprite = this.add.responsiveImage(-180 * this.game.global.scaleRation, 60 * this.game.global.scaleRation, 'life', null, Fabrique.PinnedPosition.topRight);
    this.defaultParam(sprite, this.game.global.scaleRation);
    this.lifeSpriteGroup.add(sprite);

    // life 3
    var sprite = this.add.responsiveImage(-295 * this.game.global.scaleRation, 60 * this.game.global.scaleRation, 'life', null, Fabrique.PinnedPosition.topRight);
    this.defaultParam(sprite, this.game.global.scaleRation);
    this.lifeSpriteGroup.add(sprite);

    //console.log(this.lifeSpriteGroup.countLiving());
}

// repeating task
BasicGame.Play.prototype.defaultParam = function (sprite, scaleRation) {
    sprite.anchor.setTo(0.5);
    sprite.scale.setTo(scaleRation);
}

// for debuging purpose 
BasicGame.Play.prototype.render = function () {
   // this.Balloons.render();
}