BasicGame.Score = function (game) {
    this.game = game;
}

BasicGame.Score.prototype.create = function () {
    console.log(this.game.state.getCurrentState());

    this.game.stage.backgroundColor = '#AFEBDB';

    var currentScore, highScore;
    currentScore = this.game.global.score;
    /*
    if (localStorage.getItem('Highscore') == null) {
        localStorage.setItem('Highscore', currentScore);
        highScore = currentScore;
    }
    highScore = localStorage.getItem('Highscore');
    if (currentScore > highScore) {
        localStorage.setItem('Highscore', currentScore);
    }*/
    highScore = localStorage.getItem('Highscore');
    
    if(highScore==null || undefined){
        highScore = 0;
    }
    
    var centerX = this.game.world.centerX;
    var centerY = this.game.world.centerY;
    var scaleRation = this.game.global.scaleRation;

    var ScoreText = this.game.add.bitmapText(centerX - 100, centerY - 300 * scaleRation, 'font3', 'Highscore : ' + highScore, 120);
    ScoreText.anchor.setTo(0.5);

    var button = this.game.add.responsiveButton(-5, -200 * this.game.global.scaleRation, 'menu', this.MainMenu, this, 0, 0, 0, 0, Fabrique.PinnedPosition.bottomCenter);
    button.anchor.setTo(0.5);
    button.scale.setTo(this.game.global.scaleRation+0.3);
}

BasicGame.Score.prototype.MainMenu = function(){
    this.state.start('MainScreen');
}
