BasicGame.Ballons = function (game) {
    // instance
    this.game = game;
    this.TWEEN = null;
    this.type = null;
    this.scale = this.game.global.scaleRation;
    this.worldX = this.game.world.width;
    this.worldY = this.game.world.height;
    this.ballonGroup = null;
    this.flag = false;
    this.Scoretext = null;
    this.points = 0;
}

BasicGame.Ballons.prototype.create = function (lifegroup) {
    this.createBallons(); // create balloons group
    this.lifeGroup = lifegroup;
    // add score text topRight
    this.addScoreText();

    // gameplay sound
    this.game.global.gamePlayMusic = this.game.add.audio('gamePlayMusic', 0.8, true);
    this.game.global.gamePlayMusic.play();

    // increase difficulty 
    this.game.global.VELOCITY_Y = -200;
    this.increaseDifficulty(-50);

}

BasicGame.Ballons.prototype.createBallons = function () {
    this.ballonGroup = this.game.add.physicsGroup(Phaser.Physics.ARCADE);
    //this.type = type;

    // create groups of balloon
    this.createBlnGroup();

    // poolballoons
    this.poolBalloon(this.ballonGroup);
}

BasicGame.Ballons.prototype.createBlnGroup = function () {
    var bln = {
        balloon1: {
            key: 'OB1',
            quantity: 2
        },
        balloon2: {
            key: 'OB2',
            quantity: 3
        },
        balloon3: {
            key: 'OB3',
            quantity: 2
        },
        balloon4: {
            key: 'OB4',
            quantity: 3
        },
        balloon5: {
            key: 'bomb1',
            quantity: 3
        },
        balloon6: {
            key: 'bomb2',
            quantity: 3
        },
        balloon7: {
            key: 'pointBln',
            quantity: 2
        },
        balloon8: {
            key: 'specialBln',
            quantity: 2
        }

    };

    //# created multiple balloons
    this.ballonGroup.createMultiple(bln.balloon1.quantity, bln.balloon1.key, null, false);
    this.ballonGroup.createMultiple(bln.balloon2.quantity, bln.balloon2.key, null, false);
    this.ballonGroup.createMultiple(bln.balloon3.quantity, bln.balloon3.key, null, false);
    this.ballonGroup.createMultiple(bln.balloon4.quantity, bln.balloon4.key, null, false);
    this.ballonGroup.createMultiple(bln.balloon5.quantity, bln.balloon5.key, null, false);
    this.ballonGroup.createMultiple(bln.balloon6.quantity, bln.balloon6.key, null, false);
    this.ballonGroup.createMultiple(bln.balloon7.quantity, bln.balloon7.key, null, false);
    this.ballonGroup.createMultiple(bln.balloon8.quantity, bln.balloon8.key, null, false);

    this.ballonGroup.callAll('anchor.setTo', 'anchor', 0.5, 0.5);
    this.ballonGroup.callAll('scale.setTo', 'scale', this.scale);
    this.ballonGroup.callAll('body.setSize','body',180,280,10,20);
    this.ballonGroup.setAll('checkWorldBounds', true);
    this.ballonGroup.setAll('inputEnabled', true);
}

// tapped on a balloon and kill the balloon
BasicGame.Ballons.prototype.setTouchEvent = function (sprite) {
    this.manageTappedBln(sprite, sprite.key);
    sprite.kill();
    console.log(sprite.key);
}

// balloons are tapped now check the key and return score or gameOver
BasicGame.Ballons.prototype.manageTappedBln = function (sprite, key) {

    if (key == 'bomb1' || key == 'bomb2') {
        // play bomb balloon explode sound
        var explosionSound = this.game.add.audio('bombExplode', 1, false);
        explosionSound.play();

        this.game.camera.shake(0.04, 600); // little shake here

        this.ballonGroup.forEachAlive(function (sp) {
            sp.body.velocity.setTo(0, 0);
        }, this);
        this.gameOver(sprite); // kill sprite and start aniamtions

    } else {
        this.sumScore(key, sprite);
    }
}


BasicGame.Ballons.prototype.sumScore = function (key, sprite) {
    // balloon pop sound playing here
    var balloonSound = this.game.add.audio('balloon_pop', 1, false);
    balloonSound.play();

    if (key == 'OB1' || key == 'OB2' || key == 'OB3' || key == 'OB4') {
        this.scoreTween(2, sprite.x, sprite.y);
        this.points += 2;
        this.Scoretext.text = this.points.toString();
    } else if (key == 'pointBln') {
        this.scoreTween(5, sprite.x, sprite.y);
        this.points += 5;
        this.Scoretext.text = this.points.toString();
    } else if (key == 'specialBln') {
        this.scoreTween(10, sprite.x, sprite.y);
        this.points += 10;
        this.Scoretext.text = this.points.toString();
    }
    this.game.global.score = this.points; // store the score in global
}

// little tween on score
BasicGame.Ballons.prototype.scoreTween = function (score, xx, yy) {
    var scoreLabel = this.game.add.bitmapText(xx, yy, 'font1', score.toString(), 100);
    scoreLabel.anchor.setTo(0.5);
    scoreLabel.align = 'center';
    scoreLabel.scale.setTo(this.game.global.scaleRation);

    var tweenScore = this.game.add.tween(scoreLabel);
    tweenScore.to({
        alpha: 0.5
    }, 1500, Phaser.Easing.Linear.None, true);
    tweenScore.onComplete.add(function () {
        scoreLabel.kill();
    }, this);
}

// add score instance to the game world
BasicGame.Ballons.prototype.addScoreText = function () {
    this.Scoretext = this.game.add.bitmapText(100 * this.game.global.scaleRation, 100 * this.game.global.scaleRation, 'font1', '00', 120);
    this.Scoretext.anchor.setTo(0.5);
    this.Scoretext.scale.setTo(this.game.global.scaleRation);
}


//pool balloons from dead stack
BasicGame.Ballons.prototype.poolBalloon = function (group) {
    this.addPattern(group);
}


BasicGame.Ballons.prototype.addDefaultMethod = function (sprite) {
    sprite.events.onInputDown.add(this.setTouchEvent, this);
    sprite.events.onOutOfBounds.add(this.ResetSprite, this);
}

// sprite killed when out of gameWorld
BasicGame.Ballons.prototype.ResetSprite = function (sprite) {
    if (sprite.y < -50) {
        sprite.kill();
        if (this.game.global.lifeCount <= 0) {
            this.gameOver(null);
            return;
        }
        if (sprite.key == 'bomb1' || sprite.key == 'bomb2') {
            return;
        } else {
            this.game.global.lifeCount -= 1;
            this.killLife(this.lifeGroup);
        }
    }
    console.log('life :' + this.game.global.lifeCount);
}

BasicGame.Ballons.prototype.killLife = function (Lifegroup) {
    var sp = Lifegroup.getFirstAlive();
    if (sp) {
        sp.kill();
        return;
    }
    console.log('Life : ' + Lifegroup.countAlive());
}

// forward to gameOver state
BasicGame.Ballons.prototype.gameOver = function (sprite) {
    this.game.global.gamePlayMusic.stop();

    if (sprite == null || sprite == undefined) {
        //this.game.state.clearCurrentState(); // clear state remove tween, cache etc.
        this.game.state.start('Gameover');
    } else {

        // tapped on bomb so create explosion anim and gameover
        var sp = this.game.add.sprite(sprite.x, sprite.y, 'explode');
        sp.anchor.setTo(0.5);
        sp.scale.setTo(this.game.global.scaleRation + 2);
        var anim = sp.animations.add('explosion', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]);
        sprite.kill();

        anim.onComplete.add(function () {
            //console.log('animations completed');
            this.game.state.start('Gameover');
        }, this);

        sp.animations.play('explosion', 12, false);
    }
}

BasicGame.Ballons.prototype.update = function () {
    //console.log('living : ' + this.ballonGroup.countLiving());
}


BasicGame.Ballons.prototype.increaseDifficulty = function (velocity) {
    var timer = this.game.time.create(true);
    // increase velocity each 8 seconds
    timer.loop(Phaser.Timer.SECOND * 8, function () {
        this.game.global.VELOCITY_Y += velocity;
        //console.log(this.game.global.VELOCITY_Y);
    }, this, velocity);
    timer.start();
}

BasicGame.Ballons.prototype.addPattern = function (group) {
    var timerLoop = this.game.time.create(true);

    timerLoop.loop(Phaser.Timer.SECOND * 2, function () {
        var countLiving = this.ballonGroup.countLiving();
        var countDead = this.ballonGroup.countDead();

        if (countLiving >= 0 && countLiving <= 3) {

            var rnd = this.game.rnd.integerInRange(1, 3);

            if (countDead > 10) {
                if (rnd == 1)
                    this.pattern1(this.ballonGroup);
                else if (rnd == 2)
                    this.pattern2(this.ballonGroup);
                else if (rnd == 3)
                    this.pattern3(this.ballonGroup);
                // console.log('pattern called');
            }
        }
    }, this);

    timerLoop.start();

    return;
}

BasicGame.Ballons.prototype.pattern1 = function (group) {
    /* // bug : when I add more path the bug rise    
     this.TWEEN = this.game.add.tween(sprite).to({
         x: [130, 280, 430, 280]
             //y: []
     }, 4000);

     this.TWEEN.interpolation(this.game.math.catmullRomInterpolation); // bezier and cutmullrom works fine
     this.TWEEN.start();
     */
    var temp = Math.round(this.game.world.width / 4);
    var x = this.game.world.centerX - temp;
    var y = this.game.world.height + 200;
    var velocity = this.game.global.VELOCITY_Y; // tweek this later on 


    Phaser.ArrayUtils.shuffle(group.children);
    group.updateZ();
    // sprite 1
    var sprite = group.getFirstDead();
    this.positionSprite(sprite, x, y, velocity);

    // sprite 2
    sprite = group.getFirstDead();
    this.positionSprite(sprite, x + temp * 2, y, velocity);

    // sprite 3 
    sprite = group.getFirstDead();
    this.positionSprite(sprite, x, y + sprite.height + 100, velocity);

    // sprite 4
    sprite = group.getFirstDead();
    this.positionSprite(sprite, x + temp * 2, y + sprite.height + 100, velocity);
}

BasicGame.Ballons.prototype.pattern2 = function (group) {
    var temp = Math.round(this.game.world.width / 4);
    var x = this.game.world.centerX - temp;
    var y = this.game.world.height + 200;
    var velocity = this.game.global.VELOCITY_Y; // global I will increase velocity later

    Phaser.ArrayUtils.shuffle(group.children);
    group.updateZ();

    // sprite 1
    var sprite = group.getFirstDead();
    this.positionSprite(sprite, x, y, velocity);
    // sprite 2
    sprite = group.getFirstDead();
    this.positionSprite(sprite, x + temp * 2, y, velocity);
    // sprite 3 
    sprite = group.getFirstDead();
    this.positionSprite(sprite, x, y + sprite.height + 100, velocity);
    // sprite 4
    sprite = group.getFirstDead();
    this.positionSprite(sprite, x + temp * 2, y + sprite.height + 100, velocity);
    // sprite 5
    var mid = Math.round((y + (y + sprite.height + 100)) / 2);
    sprite = group.getFirstDead();
    this.positionSprite(sprite, this.game.world.centerX, mid, velocity);

}

BasicGame.Ballons.prototype.pattern3 = function (group) {

    Phaser.ArrayUtils.shuffle(group.children);
    group.updateZ();

    var temp = Math.round(this.game.world.width / 4);
    var x = this.game.world.centerX;
    var y = this.game.world.height + 150;
    var velocity = this.game.global.VELOCITY_Y; // global I will increase velocity later
    // sprite 1
    var sprite = this.ballonGroup.getFirstDead();
    this.positionSprite(sprite, x, y, velocity);
    // sprite 2
    sprite = this.ballonGroup.getFirstDead();
    this.positionSprite(sprite, x, y + sprite.height, velocity);

    var midY = (y + y + 250) / 2;
    //sprite 3 left
    sprite = this.ballonGroup.getFirstDead();
    this.positionSprite(sprite, x - sprite.width, midY, velocity);
    // sprite 4 right
    sprite = this.ballonGroup.getFirstDead();
    this.positionSprite(sprite, x + sprite.width, midY, velocity);
}

BasicGame.Ballons.prototype.positionSprite = function (sprite, xx, yy, velocity_YY) {
    if (sprite) {
        sprite.revive();
        sprite.position.setTo(xx, yy);
        sprite.body.velocity.y = velocity_YY;
        this.addDefaultMethod(sprite);
        //console.log(sprite.key);
    }
}

BasicGame.Ballons.prototype.render = function(){
    this.ballonGroup.forEachAlive(function(sp){
        this.game.debug.body(sp);
    },this);
}