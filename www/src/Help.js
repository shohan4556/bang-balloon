BasicGame.Help = function (game) {
    this.game = game;
}

BasicGame.Help.prototype.create = function () {

    console.log(this.game.state.getCurrentState());

    this.game.stage.backgroundColor = '#60CABC';

    var TutorialText = ['Tap the balloons to score points, ', ' Don\'t let them to fly away.', ' Avoid the bomb balloons.', 'Survive as long as you can.'];

    var text = this.game.add.bitmapText(this.game.world.centerX, this.game.world.centerY - 200, 'font3', TutorialText[0] + TutorialText[1] + TutorialText[2] + TutorialText[3], 100);

    text.maxWidth = this.game.world.width - 50;
    text.wordWrap = true;
    this.setDefault(text);

    var button = this.game.add.responsiveButton(-5, -200*this.game.global.scaleRation, 'menu', this.MainMenu, this, 0, 0, 0, 0, Fabrique.PinnedPosition.bottomCenter);
    button.anchor.setTo(0.5);
    button.scale.setTo(this.game.global.scaleRation+0.3);

}

BasicGame.Help.prototype.MainMenu = function(){
    this.state.start('MainScreen');
}

BasicGame.Help.prototype.setDefault = function (text) {
    text.anchor.setTo(0.5);
    text.scale.setTo(this.game.global.scaleRation);
}