BasicGame.Gameover = function (game) {
    this.game = game;
}

BasicGame.Gameover.prototype.create = function () {
    console.log(this.game.state.getCurrentState());

    this.game.stage.backgroundColor = '#AFEBDB';

    var currentScore, highScore;
    currentScore = this.game.global.score;

    if (localStorage.getItem('Highscore') == null) {
        localStorage.setItem('Highscore', currentScore);
        highScore = currentScore;
    }
    highScore = localStorage.getItem('Highscore');
    if (currentScore > highScore) {
        localStorage.setItem('Highscore', currentScore);
    }
    highScore = localStorage.getItem('Highscore');

    //console.log('highscore : '+highScore);
    //console.log('currentScore :'+currentScore);
    var centerX = this.game.world.centerX;
    var centerY = this.game.world.centerY;
    var scaleRation = this.game.global.scaleRation;

    var ScoreText = this.game.add.bitmapText(centerX-100, centerY - 300 * scaleRation, 'font3', 'Your Score : '+currentScore, 100);
    ScoreText.anchor.setTo(0.5);
    
    ScoreText = this.game.add.bitmapText(centerX-100, centerY - 150 * scaleRation, 'font3', 'Highscore : '+highScore, 100);
    ScoreText.anchor.setTo(0.5);

    var sprite = this.add.button(this.world.centerX, this.world.centerY + 100 * this.game.global.scaleRation, 'retry', this.Retry, this);
    this.defaultParam(sprite);
}

BasicGame.Gameover.prototype.Retry = function () {
    this.game.state.start('Play');
}

BasicGame.Gameover.prototype.MainMenu = function () {
    this.game.state.start('MainScreen');
}

BasicGame.Gameover.prototype.defaultParam = function (sprite) {
    sprite.anchor.setTo(0.5);
    sprite.scale.setTo(this.game.global.scaleRation + 0.3);
}