BasicGame.Preload = function (game) {
    this.game = game;
}

BasicGame.Preload.prototype.preload = function () {
    console.log(this.state.getCurrentState());

    // loading text
    this.loadingText = this.add.bitmapText(this.world.centerX, this.world.centerY, 'font2', 'Loading ...', 120);
    this.loadingText.anchor.setTo(0.5);
    this.loadingText.scale.setTo(this.game.global.scaleRation);

    // load game assets
    this.load.image('bomb1', 'asset/bomb_01.png');
    this.load.image('bomb2', 'asset/bomb_02.png');
    this.load.image('OB1', 'asset/OB_01.png');
    this.load.image('OB2', 'asset/OB_02.png');
    this.load.image('OB3', 'asset/OB_03.png');
    this.load.image('OB4', 'asset/OB_04.png');
    this.load.image('pointBln', 'asset/point_balloon.png');
    this.load.image('specialBln', 'asset/special_balloon.png');
    this.load.image('level_01_bg', 'asset/level_01_bg.png');

    // load gui
    this.load.image('playButton', 'asset/GUI/play.png');
    this.load.image('game-title', 'asset/GUI/game-title.png');
    this.load.image('title-bg', 'asset/GUI/title-bg.png');
    this.load.image('leaderboard', 'asset/GUI/leaderboard.png');
    this.load.image('setting', 'asset/GUI/setting.png');
    this.load.image('help', 'asset/GUI/help.png');
    this.load.image('about', 'asset/GUI/credit.png');
    this.load.image('retry','asset/GUI/retry.png');
    this.load.image('menu','asset/GUI/menu.png');
    // mainScreen objects
    this.load.image('cloud1', 'asset/GUI/cloud1.png');
    this.load.image('cloud2', 'asset/GUI/cloud2.png');
    this.load.image('cloud3', 'asset/GUI/cloud3.png');
    this.load.image('cloud4', 'asset/GUI/cloud4.png');
    
    // level selection
    this.load.image('arrow1','asset/levels/arrow1.png');
    this.load.image('arrow2','asset/levels/arrow2.png');
    this.load.image('level_01','asset/level_01_bg.png');
    this.load.image('level_02','asset/levels/level_02.png');
    this.load.image('select','asset/levels/select.png');
    this.load.image('locked','asset/levels/locked.png');
    
    //objects 
    this.load.image('life', 'asset/objects/life.png');

    // animations
    this.load.spritesheet('explode', 'asset/animations/explode.png', 64, 64);

    // audio
    this.load.audio('gamePlayMusic', ['asset/Sounds/gameMusic.wav'], true);
    this.load.audio('bombExplode', ['asset/Sounds/explode.wav'], true);
    this.load.audio('balloon_pop', ['asset/Sounds/balloon_pop.wav'], true);
    // load fonts
    this.load.bitmapFont('font1', 'asset/fonts/font1.png', 'asset/fonts/font1.xml');
    this.load.bitmapFont('font3', 'asset/fonts/font3.png', 'asset/fonts/font3.xml');

}

// loading 
BasicGame.Preload.prototype.loadUpdate = function () {
    this.loadingText.text = "Loading ... " + this.game.load.progress + '%';
}

BasicGame.Preload.prototype.create = function () {
    this.game.sound.setDecodedCallback(['gamePlayMusic','bombExplode','balloon_pop'], function () {
        this.state.start('MainScreen');
    }, this);
}